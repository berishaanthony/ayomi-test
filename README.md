# Ayomi technical test

The project is built with two containers as described in the `docker-compose.yml`
- The Mysql database
- The Django project => Contains one app called `app`

The frontend is rendered using Django's template engine

---

### To build the project
`docker-compose up --build`

### Then kill the containers and do
`docker-compose up`

### Run the migrations with this command 
`sudo docker-compose exec web python3 /ayomi_test/manage.py migrate`

### Go to
[http://localhost:8000](http://localhost:8000)

---

## What has been done
- **Dockerizing** MySQL database and Django web server
- Routing and proper redirections
- Creation of a custom User class/UserManager and all related User forms :
  - Registration
  - Login
  - Modify
- Using Bootstrap components and classes

## What works
- Registration
- Login
- Change email
  - And its error handling/displaying using Bootstrap alerts + jQuery

## What doesn't work
- Displaying and handling form errors correctly => Currently creating `HTTP 500` errors
  - In case of password mismatching in RegistrationForm