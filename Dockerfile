FROM python:3

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /ayomi_test
WORKDIR /ayomi_test
#Upgrade pip
RUN pip install pip -U
ADD requirements.txt /ayomi_test/
#Install dependencies
RUN pip install -r requirements.txt
ADD . /ayomi_test/
RUN apt-get update
RUN apt-get install python3-dev default-libmysqlclient-dev  -y