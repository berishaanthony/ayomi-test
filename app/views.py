from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse, JsonResponse
from django.views import View
from .models import User, UserManager
from django.contrib.auth import authenticate, login
from django import forms


from .forms import LoginForm, RegisterForm, ModifyUserForm

# Create your views here.

class HomeView(View):
    def get(self, request):
        form = ModifyUserForm
        if request.user.is_authenticated:
            return render(request, 'home.html', {'form': form})
        else:
            return redirect(reverse('login'))

class LoginView(View):
    def get(self, request):
        form = LoginForm
        return render(request, 'login.html', {'form': form})

    def post(self, request):
        form = LoginForm(request.POST)

        if form.is_valid():
            credentials=form.cleaned_data

            user=authenticate(
                username=credentials["email"],
                password=credentials["password"]
            )
            if user is not None:
                login(request, user)
                return redirect(reverse('home'))
            else:
                return render(request, 'login.html', {'form': form})


class RegisterView(View):
    def get(self, request):
        form = RegisterForm
        return render(request, 'register.html', {'form': form})

    def post(self, request):

        form = RegisterForm(request.POST)

        if form.is_valid():
            email=form.cleaned_data.get('email')
            password=form.clean_password()
            form.save()
            user=authenticate(email=email, password=password)
            login(request, user)
            return redirect(reverse('home'))
        else:
            form = RegisterForm()
        return render(request, 'register.html', {'form': form})

class UpdateEmailView(View):
    def post(self, request):
        if not request.user.is_authenticated:
            return JsonResponse(status=401, data={'message': "An error has occurred."})

        new_email = UserManager.normalize_email(request.POST.get('new_email'))
        current_email = UserManager.normalize_email(request.POST.get('current_email'))

        if not new_email or not current_email:
            return JsonResponse(status=400, data={'message': "An error has occurred."})

        current_user = User.Objects.get(email=current_email)
        if not current_user:
            return JsonResponse(status=400, data={'message': "An error has occurred."})

        try:
            new_user_email = User.Objects.get(email=new_email)
        except User.DoesNotExist:
            current_user.email = new_email
            current_user.save()
            return JsonResponse(status=200, data={"new_email": new_email})
        return JsonResponse(status=400, data={"message": "The email is already used"})