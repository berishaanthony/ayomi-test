$(document).ready(function () {

    var displayCurrentEmail = $('#display_current_email');
    var newEmail = $('#id_new_email');
    var currentEmail = $('#id_current_email');

    
    
    $('#email_form').submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/update/",
            data: {
                'current_email': currentEmail.val(),
                'new_email': newEmail.val()
            }
        })
        .done(function(res) {
            console.log(res);
            updateCurrentEmail(displayCurrentEmail, currentEmail, res.new_email);

            $('#alert')
                .text("Your Email has been updated successfully")
                .removeClass("alert-danger")
                .addClass("alert-success")
                .show()
        })
        .fail(function(err) {
            console.log(err);
            $('#alert')
                .text(err.responseJSON.message)
                .removeClass("alert-success")
                .addClass("alert-danger")
                .show()
        })
    });

});

function updateCurrentEmail(pageDisplayEmail, modalInputEmail, newEmail) {
    if (newEmail) {
        pageDisplayEmail.text(newEmail);
        modalInputEmail.val(newEmail);
    }
}

// Setting up CSRF token
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue =   decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});