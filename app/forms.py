from django import forms
from .models import User, UserManager
class LoginForm (forms.Form):
    email = forms.EmailField(label='Email', 
                             required=True,
                             widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password',
                               required=True,
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}))
class RegisterForm(forms.ModelForm):
    password1 = forms.CharField(
        label="Password",
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
    )
    password2 = forms.CharField(
        label="Password confirmation",
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
    )

    email = forms.EmailField(label='Email', 
                             required=True,
                             widget=forms.EmailInput(attrs={'class': 'form-control'}))

    # Specifying the fields to be used by the form is necessary for security issues
    class Meta:
        model=User
        fields=("email",)


    def clean_email(self):
        email = UserManager.normalize_email(self.cleaned_data['email'])

        try:
            user = User.Objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(u'Email "%s" is already in use.' % email)

    def clean_password(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        # Handle password mismatch
        if not password2:
            raise forms.ValidationError("You must confirm your password")

        if password1 != password2:
            self._errors['password2']="Passwords don't match"
            raise forms.ValidationError("Passwords don't match")
        return password2

    # Save the user to the DB
    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
        


class ModifyUserForm(forms.Form):

    def clean_email(self):
        email = UserManager.normalize_email(self.cleaned_data['email'])

        try:
            user = User.Objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(u'Email "%s" is already in use.' % email)

    def update(self, commit=True):
        user = super(ModifyUserForm, self).save(commit=False)
        user.set_email(self.cleaned_data["new_email"])
        if commit:
            user.save()
        return user

    new_email = forms.EmailField(
        label='New email', 
        required=True,
        widget=forms.EmailInput(attrs={'class': 'form-control'})
    )
