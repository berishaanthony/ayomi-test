from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

class UserManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, email, password):
        if not email or not password:
            raise ValueError('Users must have an email and a password')
        user = self.model(
            email=self.normalize_email(email)
        )

        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(('email adress'), unique=True)
    Objects = UserManager()

    USERNAME_FIELD='email'
    REQUIRED_FIELDS=[]

    class Meta:
        verbose_name = ('user')
        verbose_name_plural = ('users')

    def __str__(self):
        return self.email